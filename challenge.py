"""
Author: Steven Taylor
Date: 13/11/19
"""

def CalculateDiscount(n,rule):
    """
    Arugments:  n   number of items
             rule   price and discount info

    Returns: cost of items with discounts applied
    """

    # discount calculation function has been
    # built to allow rule values to be changed
    # and still allow program to function
    try:
        s = rule[1].split()
        price = rule[0]
        if s[0] == "Buy":
            # Buy x, get y free
            i = int(s[1])
            j = int(s[3])
            n -= (n // (i + j)) * j
            return price * n
        elif len(s) > 3 and s[3] == "price":
            # x for the price of y
            i = int(s[0])
            j = int(s[5])
            n -= (n // i) * (i - j)
            return price * n
        elif len(s) > 1 and s[1] == "for":
            # x for y
            i = int(s[0])
            j = int(s[2])
            deal = (n // i) * j
            n = n % i
            return deal + price * n
        else:
            return price * n
    except:
        return price * n
    

class UnidaysDiscountChallenge:

    def __init__(self, pricingRules):
        self.prices = pricingRules
        self.basket = {}

    def AddToBasket(self, item):
        if item.lower() == "none":
            return
        # multiple items can be input at once
        for char in item.upper():
            # ignore items that don't exist
            if char not in self.prices:
                continue
            # tally items
            if char not in self.basket:
                self.basket[char] = 0
            self.basket[char] += 1
        return
    
    def CalculateTotalPrice(self):
        total = 0
        # Calculate prices of each item using discounts
        for item in self.basket:
            total += CalculateDiscount(self.basket[item], self.prices[item])

        deliveryCharge = 7 if 0 < total < 50 else 0
        return total,deliveryCharge

    # empty the basket
    def ClearBasket(self):
        self.basket = {}

def main():

    pricingRules = {
        "A": [8,"None"],
        "B": [12,"2 for 20"],
        "C": [4,"3 for 10"],
        "D": [7,"Buy 1 get 1 free"],
        "E": [5,"3 for the price of 2"]
    }

    order = UnidaysDiscountChallenge(pricingRules)
    loop = True

    while(loop):

        print("+==============================+")
        print("|  Unidays Discount Challenge  |")
        print("+==============================+")
        print("|      Please add an item      |")
        print("|      (A, B, C, D, or E)      |")
        print("|         to your cart         |")
        print("+==============================+\n")

        add = True

        while(add):
            items = input("Your addition(s): ")
            order.AddToBasket(items)
            try:
                q = input("Would you like to add more items (y/n)? ").lower()
            except:
                continue
            add = False if q[0] == 'n' else True
        
        result = order.CalculateTotalPrice()
        print("\nYour charge is: £",result[0],sep='')
        print("And shipping is: £",result[1],sep='')
        print("For a total of : £",sum(result),sep='')
        print("\nThank you for shopping")
        order.ClearBasket()

        try:
            ans = input("Shop again (y/n)? ").lower()
        except:
            continue 
        loop = False if ans[0] == "n" else True

    return

if __name__ == '__main__':
    main()