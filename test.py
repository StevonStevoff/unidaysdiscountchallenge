"""
Author: Steven Taylor
Date: 13/11/19
"""


import unittest
import challenge

pricingRules = {
        "A": [8,"None"],
        "B": [12,"2 for 20"],
        "C": [4,"3 for 10"],
        "D": [7,"Buy 1 get 1 free"],
        "E": [5,"3 for the price of 2"]
    }

tester = challenge.UnidaysDiscountChallenge(pricingRules)

class TestUnidaysChallenge(unittest.TestCase):

    # Test suite runs all scenarios liste
    # on GitHub page for project

    def test_none(self):
        tester.AddToBasket("none")
        total, delivery = tester.CalculateTotalPrice()
        self.assertEqual(total,0)
        self.assertEqual(delivery, 0)
        tester.ClearBasket()
    
    def test_A(self):
        tester.AddToBasket("A")
        total, delivery = tester.CalculateTotalPrice()
        self.assertEqual(total,8)
        self.assertEqual(delivery, 7)
        tester.ClearBasket()

    def test_B(self):
        tester.AddToBasket("B")
        total, delivery = tester.CalculateTotalPrice()
        self.assertEqual(total,12)
        self.assertEqual(delivery, 7)
        tester.ClearBasket()

    def test_C(self):
        tester.AddToBasket("C")
        total, delivery = tester.CalculateTotalPrice()
        self.assertEqual(total,4)
        self.assertEqual(delivery, 7)
        tester.ClearBasket()

    def test_D(self):
        tester.AddToBasket("D")
        total, delivery = tester.CalculateTotalPrice()
        self.assertEqual(total,7)
        self.assertEqual(delivery, 7)
        tester.ClearBasket()

    def test_E(self):
        tester.AddToBasket("E")
        total, delivery = tester.CalculateTotalPrice()
        self.assertEqual(total,5)
        self.assertEqual(delivery, 7)
        tester.ClearBasket()

    def test_BB(self):
        tester.AddToBasket("BB")
        total, delivery = tester.CalculateTotalPrice()
        self.assertEqual(total,20)
        self.assertEqual(delivery, 7)
        tester.ClearBasket()
    
    def test_BBB(self):
        tester.AddToBasket("BBB")
        total, delivery = tester.CalculateTotalPrice()
        self.assertEqual(total,32)
        self.assertEqual(delivery, 7)
        tester.ClearBasket()
    
    def test_BBBB(self):
        tester.AddToBasket("BBBB")
        total, delivery = tester.CalculateTotalPrice()
        self.assertEqual(total,40)
        self.assertEqual(delivery, 7)
        tester.ClearBasket()

    def test_CCC(self):
        tester.AddToBasket("CCC")
        total, delivery = tester.CalculateTotalPrice()
        self.assertEqual(total,10)
        self.assertEqual(delivery, 7)
        tester.ClearBasket()

    def test_CCCC(self):
        tester.AddToBasket("CCCC")
        total, delivery = tester.CalculateTotalPrice()
        self.assertEqual(total,14)
        self.assertEqual(delivery, 7)
        tester.ClearBasket()

    def test_DD(self):
        tester.AddToBasket("DD")
        total, delivery = tester.CalculateTotalPrice()
        self.assertEqual(total,7)
        self.assertEqual(delivery, 7)
        tester.ClearBasket()

    def test_DDD(self):
        tester.AddToBasket("DDD")
        total, delivery = tester.CalculateTotalPrice()
        self.assertEqual(total,14)
        self.assertEqual(delivery, 7)
        tester.ClearBasket()

    def test_EE(self):
        tester.AddToBasket("EE")
        total, delivery = tester.CalculateTotalPrice()
        self.assertEqual(total,10)
        self.assertEqual(delivery, 7)
        tester.ClearBasket()
    
    def test_EEE(self):
        tester.AddToBasket("EEE")
        total, delivery = tester.CalculateTotalPrice()
        self.assertEqual(total,10)
        self.assertEqual(delivery, 7)
        tester.ClearBasket()
    
    def test_EEEE(self):
        tester.AddToBasket("EEEE")
        total, delivery = tester.CalculateTotalPrice()
        self.assertEqual(total,15)
        self.assertEqual(delivery, 7)
        tester.ClearBasket()

    def test_DDDDDDDDDDDDDD(self):
        tester.AddToBasket("DDDDDDDDDDDDDD")
        total, delivery = tester.CalculateTotalPrice()
        self.assertEqual(total,49)
        self.assertEqual(delivery, 7)
        tester.ClearBasket()

    def test_BBBBCCC(self):
        tester.AddToBasket("BBBBCCC")
        total, delivery = tester.CalculateTotalPrice()
        self.assertEqual(total,50)
        self.assertEqual(delivery, 0)
        tester.ClearBasket()

    def test_ABBCCCDDEE(self):
        tester.AddToBasket("ABBCCCDDEE")
        total, delivery = tester.CalculateTotalPrice()
        self.assertEqual(total,55)
        self.assertEqual(delivery, 0)
        tester.ClearBasket()

    def test_EDCBAEDCBC(self):
        tester.AddToBasket("EDCBAEDCBC")
        total, delivery = tester.CalculateTotalPrice()
        self.assertEqual(total,55)
        self.assertEqual(delivery, 0)
        tester.ClearBasket()


if __name__ == '__main__':
	unittest.main()
