Submission for UNiDAYS intern software engineer coding challenge

----

Running this solution:
<ol>
  <li><b>Using the Test Program</b></li>
    I have built unittests which run all the cases specified in the GitHub page for the challenge.
    The unittests are written in `test.py` and can be run by typing:

        python test.py -v

  <li><b>Manual Operation</b></li>
      In order to manually test the program and add different combinations of items to the program, `challenge.py` can be run by typing:

        python challenge.py

The program prompts you here to add an item (A,B,C,D or E), and multiple can be added at a time in a string (i.e. "ABBBCC"). When you are done adding items to the list, your price, delivery charge, and total cost will be shown.</p>
</ol>

My solution to the problem involved creating a parser for the pricing rules so that in the event of one of the rules being changed (i.e. from "3 for 2" to "2 for 1"), the program would immediately recongize it and apply the change to the cart.
My cart system was built using a dictionary data structure as I felt this was the best solution for keeping track of items and their quantities.
Completed 13/11/19